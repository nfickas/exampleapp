import React, { Component } from 'react';
import { Jumbotron } from 'react-bootstrap';
import ListBox from './ListBox';

class Home extends Component {

    constructor(){
        super();

        this.state = {
            activeIndex: null,
            listOfNames: ["Nate", "Ryan", "Lindsey", "Diane", "John", "blah", "blah", "blah"]
        };

        this.handleListClick = this.handleListClick.bind(this);
    }

    handleListClick(index) {
        this.setState({ activeIndex: index })
    }
    
    render() {
        return (
            <div style={{margin: '20px'}}>
                <Jumbotron>
                    <div style={{ textAlign: 'center' }}>
                        <h1>
                            Our Example App
                        </h1>
                        Let's Learn all the things!
                    </div>
                </Jumbotron>
                <ListBox names={this.state.listOfNames} handleListClick={this.handleListClick} activeIndex={this.state.activeIndex}/>
            </div>
        );
    }
};

export default Home;