import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';

class NameItem extends Component {

    render(){
        return(
                <ListGroup.Item onClick={this.props.onClick} className={this.props.active ? 'active': ''}>
                    {this.props.name}
                </ListGroup.Item>
        );
    }
}

export default NameItem;