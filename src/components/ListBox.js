import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import NameItem from './NameItem'

class ListBox extends Component {
    
    render(){
        let active = this.props.activeIndex;
        let nameList = (this.props.names).map((name, index) => <NameItem name={name} active={index === active} onClick={this.props.handleListClick.bind(null, index)} key={name}/>);

        return(
            <div style={{width: 200, maxHeight: 250, overflow: 'auto'}}>
                <ListGroup>
                    {nameList}
                </ListGroup>
            </div>
        );
    }
}

export default ListBox;